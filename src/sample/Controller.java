package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
Filechooser filechooser = new FileChooser();
    @FXML
    private Button Event;
    @FXML
    private Button Event1;

    @FXML
    void pressbutton(ActionEvent event) throws IOException {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("JAR.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("JAR window");
            stage.setScene(new Scene(root1));
            stage.show();
            FileChooser.setInitialDirectory (new File( "C:\\temp"));
        }
        catch (Exception e){
            System.out.println("cant load");
        }

        };
    @FXML
    void pressbutton1(ActionEvent event) throws IOException {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SHH.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("SHH window");
            stage.setScene(new Scene(root1));
            stage.show();
        }
        catch (Exception e){
            System.out.println("cant load");
        }

    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}




